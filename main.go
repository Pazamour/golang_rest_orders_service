package main

import (
	"CRUDDynamoDBRESTAPI1/cache"
	"CRUDDynamoDBRESTAPI1/controller"
	"CRUDDynamoDBRESTAPI1/repository"
	"CRUDDynamoDBRESTAPI1/router"
	"CRUDDynamoDBRESTAPI1/service"
	"fmt"
	"net/http"
)

var (
	orderRepository repository.OrdersRepository = repository.NewDynamoDBRepository()
	orderService    service.OrderService        = service.NewOrderService(orderRepository)
	orderCache      cache.OrdersCache           = cache.NewRedisCache("localhost:6379", 10, 100)
	orderController controller.OrderController  = controller.NewOrderController(orderService, orderCache)
	//orderController controller.OrderController = controller.NewOrderController(orderService)
	httpRouter router.Router = router.NewMuxRouter()
)

func main() {
	const port string = ":8080"

	httpRouter.GET("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Up and Running...")
	})

	httpRouter.GET("/orders", orderController.GetOrders)
	httpRouter.POST("/orders", orderController.AddOrder)
	httpRouter.GET("/orders/{id}", orderController.GetOrderByID)
	httpRouter.PUT("/orders/{id}", orderController.UpdateOrder)
	httpRouter.DELETE("/orders/{id}", orderController.DeleteOrder)
	httpRouter.SERVE(port)
}
