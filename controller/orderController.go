package controller

import (
	"CRUDDynamoDBRESTAPI1/cache"
	"CRUDDynamoDBRESTAPI1/entity"
	"CRUDDynamoDBRESTAPI1/service"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"
)

var (
	orderService service.OrderService
	orderCache   cache.OrdersCache
)

type OrderController interface {
	GetOrders(response http.ResponseWriter, request *http.Request)
	AddOrder(response http.ResponseWriter, request *http.Request)
	GetOrderByID(response http.ResponseWriter, request *http.Request)
	DeleteOrder(response http.ResponseWriter, request *http.Request)
	UpdateOrder(response http.ResponseWriter, request *http.Request)
}

type controller struct {
}

//func NewOrderController(service service.OrderService) OrderController {
func NewOrderController(service service.OrderService, cache cache.OrdersCache) OrderController {
	orderService = service
	orderCache = cache
	return &controller{}
}

func (*controller) GetOrders(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	orders, err := orderService.FindAll()

	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(errors.New("Error getting the orders"))
		return
	}

	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode(orders)
}

func (*controller) AddOrder(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	var order entity.Order
	err := json.NewDecoder(request.Body).Decode(&order)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(errors.New("Error Unmarshalling the order data"))
		return
	}
	// json.NewEncoder(response).Encode(order) Till Here the code is correct

	err1 := orderService.Validate(&order)
	if err1 != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(errors.New(err1.Error()))
		return
	}

	//json.NewEncoder(response).Encode(order) //////Till Here the code is correct

	result, err2 := orderService.Create(&order)
	if err2 != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(errors.New("Error adding the order data"))
		return
	}

	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode(result)

}

func (*controller) GetOrderByID(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	orderID := strings.Split(request.URL.Path, "/")[2]

	fmt.Println(" controller: ", orderID, reflect.TypeOf(orderID))

	var order1 *entity.Order = orderCache.Get(orderID)
	if order1 == nil {
		order, err := orderService.FindByID(orderID)
		if err != nil {
			response.WriteHeader(http.StatusNotFound)
			json.NewEncoder(response).Encode(errors.New("No order found!"))
			return
		}
		orderCache.Set(orderID, order)
		response.WriteHeader(http.StatusOK)
		json.NewEncoder(response).Encode(order)
	} else {
		response.WriteHeader(http.StatusOK)
		json.NewEncoder(response).Encode(order1)
	}

	/*order, err := orderService.FindByID(orderID)

	if err != nil {
		response.WriteHeader(http.StatusNotFound)
		json.NewEncoder(response).Encode(errors.New("No order found!"))
		return
	}

	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode(order)
	*/
}

func (*controller) DeleteOrder(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	orderID := strings.Split(request.URL.Path, "/")[2]

	fmt.Println(" ", orderID, reflect.TypeOf(orderID))

	err1 := orderService.Delete(orderID)
	if err1 != nil {
		response.WriteHeader(http.StatusNotFound)
		json.NewEncoder(response).Encode(errors.New("Order Deletion Failed!"))
		return
	}
	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode("Order Deleted Succesfully")
}

func (*controller) UpdateOrder(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	orderID := strings.Split(request.URL.Path, "/")[2]

	fmt.Println(" ", orderID, reflect.TypeOf(orderID))

	var newOrder entity.Order
	newOrder.Id = orderID
	err := json.NewDecoder(request.Body).Decode(&newOrder)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(response).Encode(errors.New("Error Unmarshalling the order data"))
		return
	}

	result, err1 := orderService.Update(orderID, &newOrder)
	if err1 != nil {
		response.WriteHeader(http.StatusNotFound)
		json.NewEncoder(response).Encode(errors.New("Order Updation Failed!"))
		return
	}

	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode(result)
}
