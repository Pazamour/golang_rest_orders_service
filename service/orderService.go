package service

import (
	"CRUDDynamoDBRESTAPI1/entity"
	"CRUDDynamoDBRESTAPI1/kafka"
	"CRUDDynamoDBRESTAPI1/repository"
	"errors"
	"fmt"
	"math/rand"
	"reflect"
	"strconv"
)

type OrderService interface {
	Validate(order *entity.Order) error
	Create(order *entity.Order) (*entity.Order, error)
	FindAll() ([]entity.Order, error)
	FindByID(id string) (*entity.Order, error)
	Delete(orderId string) error
	Update(oldOrderId string, newOrder *entity.Order) (*entity.Order, error)
}

type service struct {
}

var (
	repo repository.OrdersRepository
)

func NewOrderService(repository repository.OrdersRepository) OrderService {
	repo = repository
	return &service{}

}

func (*service) Validate(order *entity.Order) error {
	if order == nil {
		err := errors.New("The order is empty")
		return err
	}

	if order.ItemName == "" {
		err := errors.New("ItemName in the order is empty")
		return err
	}
	return nil
}

func (*service) Create(order *entity.Order) (*entity.Order, error) {
	id := rand.Intn(200)
	order.Id = strconv.Itoa(id)
	kafka.SaveOrderToKafka(*order)
	return repo.Save(order)
}

func (*service) FindAll() ([]entity.Order, error) {
	return repo.FindAll()
}

func (*service) FindByID(id string) (*entity.Order, error) {
	_, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil, err
	}
	fmt.Println(" service: ", id, reflect.TypeOf(id))
	return repo.FindById(id)
}

func (*service) Delete(orderId string) error {
	order, err := repo.FindById(orderId)
	if err != nil {
		return err
	}
	fmt.Println(" ", orderId, reflect.TypeOf(orderId))
	return repo.Delete(order)
}

func (*service) Update(oldOrderId string, newOrder *entity.Order) (*entity.Order, error) {

	oldOrder, err := repo.FindById(oldOrderId)
	if err != nil {
		return &entity.Order{}, err
	}

	err1 := repo.Delete(oldOrder)
	if err1 != nil {
		return oldOrder, err1
	}

	return repo.Save(newOrder)
}
