package service

import (
	"CRUDDynamoDBRESTAPI1/entity"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

type MockRepository struct {
	mock.Mock
}

func (mock *MockRepository) Save(order *entity.Order) (*entity.Order, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(*entity.Order), args.Error(1)
}

func (mock *MockRepository) FindAll() ([]entity.Order, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.([]entity.Order), args.Error(1)
}

func (mock *MockRepository) FindById(id string) (*entity.Order, error) {
	return &entity.Order{}, nil
}

func (mock *MockRepository) Delete(order *entity.Order) error {
	return nil
}


func TestCreate(t *testing.T) {
	mockRepo := new(MockRepository)
	order := entity.Order{ ItemName: "Dosa", Price: "250"}

	mockRepo.On("Save").Return(&order, nil)


	testService := NewOrderService(mockRepo)
	result, err := testService.Create(&order)

	mockRepo.AssertExpectations(t)

	assert.Nil(t, err)
	assert.NotNil(t, result.Id)
	assert.Equal(t, "Dosa", result.ItemName)
	assert.Equal(t, "250", result.Price)

}

func TestFindAll(t *testing.T) {
	mockRepo := new(MockRepository)
	order := entity.Order{ Id: "1", ItemName: "Dosa", Price: "250"}

	mockRepo.On("FindAll").Return([]entity.Order{order}, nil)


	testService := NewOrderService(mockRepo)
	result, err := testService.FindAll()

	mockRepo.AssertExpectations(t)

	assert.Nil(t, err)
	assert.Equal(t, "1", result[0].Id)
	assert.Equal(t, "Dosa", result[0].ItemName)
	assert.Equal(t, "250", result[0].Price)
}

func TestValidateEmptyOrder(t *testing.T) {
	testService := NewOrderService(nil)
	err := testService.Validate(nil)

	assert.NotNil(t, err)
	assert.Equal(t, "The order is empty", err.Error())
}

func TestValidateEmptyOrderItemName(t *testing.T) {
	order := entity.Order{"1", "", "250"}
	testService := NewOrderService(nil)
	err := testService.Validate(&order)

	assert.NotNil(t, err)
	assert.Equal(t, "ItemName in the order is empty", err.Error())
}