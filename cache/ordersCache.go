package cache

import "CRUDDynamoDBRESTAPI1/entity"

type OrdersCache interface {
	Set(key string, value *entity.Order)
	Get(key string) *entity.Order
}
