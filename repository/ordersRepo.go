package repository

import "CRUDDynamoDBRESTAPI1/entity"

type OrdersRepository interface {
	Save(order *entity.Order) (*entity.Order, error)
	FindAll() ([]entity.Order, error)
	FindById(id string) (*entity.Order, error)
	Delete(order *entity.Order) error
	//Update(order *entity.Order) (*entity.Order, error)
}
