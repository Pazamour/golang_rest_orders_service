package entity

type Order struct {
	Id       string `json:"Id"`
	ItemName string `json:"ItemName"`
	Price    string `json:"Price"`
}
